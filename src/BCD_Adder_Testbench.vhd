library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_unsigned.all;
entity bcd_adder_tb is
end bcd_adder_tb;
architecture bcd_adder_tb_arch of bcd_adder_tb is
    component n_bit_bcd_adder
	generic (N : integer := 8);
	port (
		a : in STD_LOGIC_VECTOR (N-1 downto 0);
		b : in STD_LOGIC_VECTOR (N-1 downto 0);
		y : out STD_LOGIC_VECTOR (N-1 downto 0)
		);
    end component;
	signal a : STD_LOGIC_VECTOR (7 downto 0) := (Others => '0');
	signal b : STD_LOGIC_VECTOR (7 downto 0) := (Others => '0');
	signal y : STD_LOGIC_VECTOR (7 downto 0) := (Others => '0');
	begin
		n_bit_bcd_adder_0 : n_bit_bcd_adder port map (
		a => a,
		b => b,
		y => y
		);
		process
			begin
			wait for 10 ns;
			a <= "00000001";
			b <= "00000000";
			wait for 10 ns;
			a <= "00000011";
			b <= "00000001";
			wait for 10 ns;
			a <= "00100001";
			b <= "00001000";
			wait for 10 ns;
			a <= "11101001";
			b <= "00111000";
			wait for 10 ns;
			a <= "00001000";
			b <= "01001111";
			wait for 10 ns;
			a <= "01110111";
			b <= "00111000";
			wait for 10 ns;
			a <= "00010011";
			b <= "10101110";
			wait for 10 ns;
			a <= "01101001";
			b <= "01001011";
			wait for 10 ns;
			a <= "01101101";
			b <= "00111000";
			wait for 10 ns;
			a <= "11110110";
			b <= "11110011";
			wait for 10 ns;
			a <= "00110100";
			b <= "00011101";
			wait for 10 ns;
			a <= "11000101";
			b <= "11101111";
			wait for 10 ns;
			a <= "11010111";
			b <= "11110000";
			wait for 10 ns;
			a <= "10101101";
			b <= "11110111";
			wait for 10 ns;
			a <= "10000001";
			b <= "01100010";
			wait for 10 ns;
			a <= "01000001";
			b <= "10000011";
			wait for 10 ns;
			a <= "10011011";
			b <= "10110000";
			wait for 10 ns;
			a <= "00001011";
			b <= "10100101";
			wait for 10 ns;
			a <= "11000011";
			b <= "10101100";
			wait for 10 ns;
			a <= "10101010";
			b <= "01110100";
			wait for 10 ns;
			a <= "11101101";
			b <= "01111000";
			wait for 10 ns;
			a <= "10101000";
			b <= "11010011";
			wait for 10 ns;
			a <= "10111111";
			b <= "11110110";
			wait for 10 ns;
			a <= "00101001";
			b <= "00111001";
			wait for 10 ns;
			a <= "00111010";
			b <= "11000000";
			wait for 10 ns;
			a <= "00001010";
			b <= "11000011";
			wait for 10 ns;
			a <= "00111111";
			b <= "00011100";
			wait for 10 ns;
			a <= "00100011";
			b <= "11011010";
			wait for 10 ns;
			a <= "01011111";
			b <= "10111101";
			wait for 10 ns;
		end process;
end bcd_adder_tb_arch;