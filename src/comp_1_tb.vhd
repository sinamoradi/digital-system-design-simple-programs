LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.all; 
ENTITY tb_comparator_VHDL IS
END tb_comparator_VHDL;
 
ARCHITECTURE behavior OF tb_comparator_VHDL IS 
 
    COMPONENT comparator_VHDL
    PORT(
         A : IN  std_logic_vector(1 downto 0);
         B : IN  std_logic_vector(1 downto 0);
         A_kamtar_B : OUT  std_logic;
         A_barabar_B : OUT  std_logic;
         A_bishtar_B : OUT  std_logic
        );
    END COMPONENT;
   signal A : std_logic_vector(1 downto 0) := (others => '0');
   signal B : std_logic_vector(1 downto 0) := (others => '0');
   signal A_kamtar_B : std_logic;
   signal A_barabar_B : std_logic;
   signal A_bishtar_B : std_logic;
BEGIN
   uut: comparator_VHDL PORT MAP (
          A => A,
          B => B,
          A_kamtar_B => A_kamtar_B,
          A_barabar_B => A_barabar_B,
          A_bishtar_B => A_bishtar_B
        );
   stim_proc: process
   begin 
    A <= "00";  
    B <= "01"; 
    wait for 20 ns;
    A <= "11";  
    B <= "11"; 
    wait for 20 ns;
    A <= "10";  
    B <= "01"; 
    wait for 20 ns;
   end process;
END;