library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_unsigned.all;
entity n_bit_bcd_adder is
	generic (N : integer);
	port (
		a : in STD_LOGIC_VECTOR (N-1 downto 0);
		b : in STD_LOGIC_VECTOR (N-1 downto 0);
		y : out STD_LOGIC_VECTOR (N-1 downto 0)
		);
end n_bit_bcd_adder;
architecture bcd_adder_arch of n_bit_bcd_adder is
	begin
		process(a, b)
			begin
			y <= a + b;
		end process;
end bcd_adder_arch;